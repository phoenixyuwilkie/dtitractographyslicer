set(proj ANTs)

# Set dependency list
set(${proj}_DEPENDENCIES "")

# Sanity checks
if(DEFINED ANT_INCLUDE_DIR AND NOT EXISTS ${ANT_INCLUDE_DIR})
  message(FATAL_ERROR "ANT_INCLUDE_DIR variable is defined but corresponds to nonexistent directory")
endif()
if(DEFINED ANT_LIBRARY AND NOT EXISTS ${ANT_LIBRARY})
  message(FATAL_ERROR "ANT_LIBRARY variable is defined but corresponds to nonexistent path")
endif()

if(${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj})
  unset(ANT_INCLUDE_DIR CACHE)
  unset(ANT_LIBRARY CACHE)
  find_package(ANT REQUIRED MODULE)
endif()

# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj DEPENDS_VAR ${proj}_DEPENDENCIES)

if((NOT ANT_INCLUDE_DIR OR NOT ANT_LIBRARY)
   AND NOT ${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj})

  set(_version "1.0.16")

  ExternalProject_Add(${proj}
    ${${proj}_EP_ARGS}
    URL "https://bitbucket.org/hardisty/ants.git"
#sct    URL "https://bitbucket.org/OrthopaedicBiomechanicsLab/spinalcordtoolbox"
#    URL_MD5 "e794de272919ad130300dc471652aeab"		# MD5 is deprecated
    DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR}
    SOURCE_DIR ${proj}
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
    DEPENDS
      ${${proj}_DEPENDENCIES}
    )

  ExternalProject_GenerateProjectDescription_Step(${proj}
    VERSION ${_version}
    )

  set(ANT_DIR ${CMAKE_BINARY_DIR}/${proj})

  set(ANT_INCLUDE_DIR "${ANT_DIR}/headers")

# not sure about this computer OS stuff
  if(WIN32)
    set(ANT_LIBRARY "${ANTs_DIR}/lib/win64/ants_api.lib")
  elseif(APPLE)
    set(ANT_LIBRARY "${ANT_DIR}/bin/osx64/ANTs.framework")
  elseif(UNIX)
    set(ANT_LIBRARY "${ANT_DIR}/bin/linux64/libants_api.so")
  endif()
  mark_as_superbuild(SCT_LIBRARY:FILEPATH)

  #-----------------------------------------------------------------------------
  # Launcher setting specific to build tree

  if(WIN32)
    set(_dir "${ANTs_DIR}/bin/win64")
  elseif(APPLE)
    set(_dir "${ANTs_DIR}/bin/osx64")
  elseif(UNIX)
    set(_dir "${ANTs_DIR}/bin/linux64")
  endif()
  set(${proj}_LIBRARY_PATHS_LAUNCHER_BUILD ${_dir})
  mark_as_superbuild(
    VARS ${proj}_LIBRARY_PATHS_LAUNCHER_BUILD
    LABELS "LIBRARY_PATHS_LAUNCHER_BUILD"
    )

else()
  ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDENCIES})
endif()

ExternalProject_Message(${proj} "ANT_INCLUDE_DIR:${ANT_INCLUDE_DIR}")
ExternalProject_Message(${proj} "ANT_LIBRARY:${ANT_LIBRARY}")
