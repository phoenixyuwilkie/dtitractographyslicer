__version_info__ = (
  4,
  10,
  0,
  0
)

__version__ = ".".join(map(str, __version_info__))
