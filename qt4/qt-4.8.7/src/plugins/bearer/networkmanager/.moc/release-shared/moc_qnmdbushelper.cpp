/****************************************************************************
** Meta object code from reading C++ file 'qnmdbushelper.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qnmdbushelper.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qnmdbushelper.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QNmDBusHelper[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   15,   14,   14, 0x05,
      54,   15,   14,   14, 0x05,
     103,   15,   14,   14, 0x05,
     154,   15,   14,   14, 0x05,
     211,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
     243,   14,   14,   14, 0x0a,
     271,   14,   14,   14, 0x0a,
     309,   14,   14,   14, 0x0a,
     349,   14,   14,   14, 0x0a,
     395,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QNmDBusHelper[] = {
    "QNmDBusHelper\0\0,\0pathForStateChanged(QString,quint32)\0"
    "pathForAccessPointAdded(QString,QDBusObjectPath)\0"
    "pathForAccessPointRemoved(QString,QDBusObjectPath)\0"
    "pathForPropertiesChanged(QString,QMap<QString,QVariant>)\0"
    "pathForSettingsRemoved(QString)\0"
    "deviceStateChanged(quint32)\0"
    "slotAccessPointAdded(QDBusObjectPath)\0"
    "slotAccessPointRemoved(QDBusObjectPath)\0"
    "slotPropertiesChanged(QMap<QString,QVariant>)\0"
    "slotSettingsRemoved()\0"
};

void QNmDBusHelper::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QNmDBusHelper *_t = static_cast<QNmDBusHelper *>(_o);
        switch (_id) {
        case 0: _t->pathForStateChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint32(*)>(_a[2]))); break;
        case 1: _t->pathForAccessPointAdded((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QDBusObjectPath(*)>(_a[2]))); break;
        case 2: _t->pathForAccessPointRemoved((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QDBusObjectPath(*)>(_a[2]))); break;
        case 3: _t->pathForPropertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[2]))); break;
        case 4: _t->pathForSettingsRemoved((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->deviceStateChanged((*reinterpret_cast< quint32(*)>(_a[1]))); break;
        case 6: _t->slotAccessPointAdded((*reinterpret_cast< QDBusObjectPath(*)>(_a[1]))); break;
        case 7: _t->slotAccessPointRemoved((*reinterpret_cast< QDBusObjectPath(*)>(_a[1]))); break;
        case 8: _t->slotPropertiesChanged((*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[1]))); break;
        case 9: _t->slotSettingsRemoved(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QNmDBusHelper::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QNmDBusHelper::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QNmDBusHelper,
      qt_meta_data_QNmDBusHelper, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QNmDBusHelper::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QNmDBusHelper::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QNmDBusHelper::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QNmDBusHelper))
        return static_cast<void*>(const_cast< QNmDBusHelper*>(this));
    if (!strcmp(_clname, "QDBusContext"))
        return static_cast< QDBusContext*>(const_cast< QNmDBusHelper*>(this));
    return QObject::qt_metacast(_clname);
}

int QNmDBusHelper::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void QNmDBusHelper::pathForStateChanged(const QString & _t1, quint32 _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QNmDBusHelper::pathForAccessPointAdded(const QString & _t1, QDBusObjectPath _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QNmDBusHelper::pathForAccessPointRemoved(const QString & _t1, QDBusObjectPath _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QNmDBusHelper::pathForPropertiesChanged(const QString & _t1, QMap<QString,QVariant> _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QNmDBusHelper::pathForSettingsRemoved(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
