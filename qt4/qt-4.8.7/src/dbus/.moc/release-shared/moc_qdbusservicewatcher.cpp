/****************************************************************************
** Meta object code from reading C++ file 'qdbusservicewatcher.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qdbusservicewatcher.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdbusservicewatcher.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QDBusServiceWatcher[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       2,   34, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      29,   21,   20,   20, 0x05,
      56,   21,   20,   20, 0x05,
     111,   85,   20,   20, 0x05,

 // slots: signature, parameters, type, tag, flags
     159,  156,   20,   20, 0x08,

 // properties: name, type, flags
     219,  207, 0x0b095103,
     245,  235, 0x0009510b,

       0        // eod
};

static const char qt_meta_stringdata_QDBusServiceWatcher[] = {
    "QDBusServiceWatcher\0\0service\0"
    "serviceRegistered(QString)\0"
    "serviceUnregistered(QString)\0"
    "service,oldOwner,newOwner\0"
    "serviceOwnerChanged(QString,QString,QString)\0"
    ",,\0_q_serviceOwnerChanged(QString,QString,QString)\0"
    "QStringList\0watchedServices\0WatchMode\0"
    "watchMode\0"
};

void QDBusServiceWatcher::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QDBusServiceWatcher *_t = static_cast<QDBusServiceWatcher *>(_o);
        switch (_id) {
        case 0: _t->serviceRegistered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->serviceUnregistered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->serviceOwnerChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 3: _t->d_func()->_q_serviceOwnerChanged((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QDBusServiceWatcher::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QDBusServiceWatcher::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QDBusServiceWatcher,
      qt_meta_data_QDBusServiceWatcher, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QDBusServiceWatcher::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QDBusServiceWatcher::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QDBusServiceWatcher::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QDBusServiceWatcher))
        return static_cast<void*>(const_cast< QDBusServiceWatcher*>(this));
    return QObject::qt_metacast(_clname);
}

int QDBusServiceWatcher::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QStringList*>(_v) = watchedServices(); break;
        case 1: *reinterpret_cast< WatchMode*>(_v) = watchMode(); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setWatchedServices(*reinterpret_cast< QStringList*>(_v)); break;
        case 1: setWatchMode(*reinterpret_cast< WatchMode*>(_v)); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDBusServiceWatcher::serviceRegistered(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDBusServiceWatcher::serviceUnregistered(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QDBusServiceWatcher::serviceOwnerChanged(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
