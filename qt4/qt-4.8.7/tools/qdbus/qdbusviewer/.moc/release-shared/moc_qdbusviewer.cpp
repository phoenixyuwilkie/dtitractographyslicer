/****************************************************************************
** Meta object code from reading C++ file 'qdbusviewer.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qdbusviewer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdbusviewer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QDBusViewer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      23,   12,   12,   12, 0x0a,
      37,   31,   12,   12, 0x08,
      65,   12,   12,   12, 0x08,
      93,   89,   12,   12, 0x08,
     127,   89,   12,   12, 0x08,
     152,   89,   12,   12, 0x08,
     178,   89,   12,   12, 0x08,
     208,  204,   12,   12, 0x08,
     234,   12,   12,   12, 0x08,
     260,  252,   12,   12, 0x08,
     287,  252,   12,   12, 0x08,
     339,  316,   12,   12, 0x08,
     389,  384,   12,   12, 0x08,
     411,  204,   12,   12, 0x08,
     433,  429,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_QDBusViewer[] = {
    "QDBusViewer\0\0refresh()\0about()\0index\0"
    "serviceChanged(QModelIndex)\0"
    "showContextMenu(QPoint)\0sig\0"
    "connectionRequested(BusSignature)\0"
    "callMethod(BusSignature)\0"
    "getProperty(BusSignature)\0"
    "setProperty(BusSignature)\0msg\0"
    "dumpMessage(QDBusMessage)\0refreshChildren()\0"
    "service\0serviceRegistered(QString)\0"
    "serviceUnregistered(QString)\0"
    "name,oldOwner,newOwner\0"
    "serviceOwnerChanged(QString,QString,QString)\0"
    "item\0activate(QModelIndex)\0logError(QString)\0"
    "url\0anchorClicked(QUrl)\0"
};

void QDBusViewer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QDBusViewer *_t = static_cast<QDBusViewer *>(_o);
        switch (_id) {
        case 0: _t->refresh(); break;
        case 1: _t->about(); break;
        case 2: _t->serviceChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 3: _t->showContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 4: _t->connectionRequested((*reinterpret_cast< const BusSignature(*)>(_a[1]))); break;
        case 5: _t->callMethod((*reinterpret_cast< const BusSignature(*)>(_a[1]))); break;
        case 6: _t->getProperty((*reinterpret_cast< const BusSignature(*)>(_a[1]))); break;
        case 7: _t->setProperty((*reinterpret_cast< const BusSignature(*)>(_a[1]))); break;
        case 8: _t->dumpMessage((*reinterpret_cast< const QDBusMessage(*)>(_a[1]))); break;
        case 9: _t->refreshChildren(); break;
        case 10: _t->serviceRegistered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->serviceUnregistered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->serviceOwnerChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 13: _t->activate((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 14: _t->logError((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->anchorClicked((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QDBusViewer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QDBusViewer::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_QDBusViewer,
      qt_meta_data_QDBusViewer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QDBusViewer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QDBusViewer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QDBusViewer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QDBusViewer))
        return static_cast<void*>(const_cast< QDBusViewer*>(this));
    return QWidget::qt_metacast(_clname);
}

int QDBusViewer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
